package query_builder

import (
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"strings"
	"time"
)

var conditions = map[string]string{"and": "$and", "or": "$or"}
var operators = map[string]string{
	"=":        "$eq",
	"!=":       "$ne",
	"<":        "$lt",
	"<=":       "$lte",
	">":        "$gt",
	">=":       "$gte",
	"in":       "$in",
	"not in":   "$nin",
	"contains": "$regex",
}

type QueryParser[T any] struct {
}

func (q QueryParser[T]) validate(field string) (ok bool, error error) {
	okStr, error := GetTagFromStruct(field, *new(T), "filterable")
	if error != nil {
		return false, error
	}
	return okStr == "true", nil
}
func (q QueryParser[T]) mapRule(rule *Rule) (query bson.M, err error) {

	field := rule.Field
	bsonValue, err := GetTagFromStruct(field, *new(T), "bson")

	ok, err := q.validate(field)

	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, fmt.Errorf("Can't fillter %s", field)
	}
	var value any
	okStr, error := GetTypeFromStruct(field, *new(T))
	if error != nil {
		return nil, error
	}
	if okStr == "*time.Time" || okStr == "time.Time" {
		value, err = time.Parse(time.RFC3339, rule.Value.(string))
		if err != nil {
			return nil, err
		}
	} else {
		value = rule.Value
	}

	operator, ok := operators[*rule.Operator]
	if !ok {
		operator = "$eq"
	}
	//var query bson.M
	field = strings.TrimSpace(strings.Split(bsonValue.(string), ",")[0])
	if operator == "$regex" {
		query = bson.M{
			field: bson.M{operator: value, "$options": "i"},
		}
	} else {
		query = bson.M{
			field: bson.M{operator: value},
		}
	}
	return query, nil
}
func (q QueryParser[T]) mapRuleSet(ruleSet *Rule) (query bson.M, errors []error) {
	if len(ruleSet.Rules) == 0 {
		return bson.M{}, nil
	}
	querys := []bson.M{}
	for _, rule := range ruleSet.Rules {
		if rule.Operator != nil {
			result, err := q.mapRule(rule)
			if err != nil {
				errors = append(errors, err)
				continue
			}
			querys = append(querys, result)
		} else {

			result, errs := q.mapRuleSet(rule)
			querys = append(querys, result)
			errors = append(errors, errs...)
		}
	}
	if len(querys) == 0 {
		return nil, errors
	}
	query = bson.M{
		conditions[ruleSet.Condition]: querys,
	}
	return
}
func (q QueryParser[T]) Parser(queryString string) (query bson.M, errors []error) {
	p := new(QueryParser[T])
	rule := new(Rule)
	json.Unmarshal([]byte(queryString), rule)
	return p.mapRuleSet(rule)
}

func NewQueryBuilderParser[T any]() *QueryParser[T] {

	return &QueryParser[T]{}

}
