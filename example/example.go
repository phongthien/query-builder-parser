package main

import (
	query_builder "gitlab.com/phongthien/query-builder-parser"
	"log"
)

type Test struct {
	RecordType string `json:"name"  bson:"recordType,omitempty" sortable:"true" filterable:"true"`
	Items      []Item `json:"name"  bson:"Items" sortable:"true" filterable:"true"`
}
type Item struct {
	Address string `json:"address" bson:"address "sortable:"true" filterable:"true"`
	Name    string `json:"name" bson:"name" sortable:"true"`
}

func main() {
	s := `{
	 "condition": "and",
	 "rules": [{
	   "field": "RecordType",
	   "operator": "=",
	   "value": "Item"
	 }]
	}`
	p := query_builder.QueryParser[Test]{}
	result, err := p.Parser(s)
	log.Print(result, err)
	//getTagFromStruct("Items.Name", Test{}, "filterable")

}
